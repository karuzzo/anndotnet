﻿//////////////////////////////////////////////////////////////////////////////////////////
// ANNdotNET - Deep Learning Tool on .NET Platform                                      //
// Copyright 2017-2018 Bahrudin Hrnjica                                                 //
//                                                                                      //
// This code is free software under the MIT License                                     //
// See license section of  https://github.com/bhrnjica/anndotnet/blob/master/LICENSE.md //
//                                                                                      //
// Bahrudin Hrnjica                                                                     //
// bhrnjica@hotmail.com                                                                 //
// Bihac, Bosnia and Herzegovina                                                        //
// http://bhrnjica.net                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////
using anndotnet.wnd.Models;
using ANNdotNET.Core;
using ANNdotNET.Lib;
using DataProcessing.Wnd;
using NNetwork.Core.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using NetMQ;
using NetMQ.Sockets;

namespace anndotnet.wnd.Panels
{
    /// <summary>
    /// Interaction logic for Test.xaml
    /// </summary>
    public partial class IPCServer : UserControl
    {
        private CancellationTokenSource m_Cts = new CancellationTokenSource();
        private Thread m_Thread;
        private ResponseSocket server;
        string port = "";

        public IPCServer()
        {
            InitializeComponent();
            //this.DataContextChanged += StartServer_DataContextChanged;
        }



        private void StartServer_BtnClick(object sender, RoutedEventArgs e)
        {
            port = TextBoxPort.Text;

            MLConfigController model = (MLConfigController)DataContext;

            if (m_Thread == null || !m_Thread.IsAlive)
            {
                m_Cts = new CancellationTokenSource();
                m_Thread = new Thread(() => Listen(model, m_Cts.Token))
                {
                    IsBackground = true
                };
                m_Thread.Start();
            }

            LabelStatus.Content = "Server status: Enabled";
        }

        private void StopServer_BtnClick(object sender, RoutedEventArgs e)
        {
            m_Cts.Cancel();

            if (server != null)
            {
                server.Disconnect("@tcp://localhost:" + port);
                server.Dispose();
            }

            LabelStatus.Content = "Server status: Disabled";
        }

        private void Listen(MLConfigController model, CancellationToken token)
        {

            //check if the trained model exists
            if (string.IsNullOrEmpty(model.TrainingParameters.LastBestModel) || string.IsNullOrEmpty(model.TrainingParameters.LastBestModel.Trim(' ')))
            {
                throw new Exception("There is no model to test. Model must be trained first.");
            }

            //Load ML model configuration file
            var modelPath = Project.GetMLConfigPath(model.Settings, model.Name);
            var dicMParameters = Project.LoadMLConfig(modelPath);
            var trainedModelRelativePath = Project.GetParameterValue(dicMParameters["training"], "TrainedModel");
            
            //add full path of model folder since model file doesn't contains any apsolute path
            dicMParameters.Add("root", Project.GetMLConfigFolder(modelPath));
            var strModelToEvaluatePath = $"{dicMParameters["root"]}\\{trainedModelRelativePath}";
            ProcessDevice pd = ProcessDevice.Default;

            ResponseSocket server = new ResponseSocket("@tcp://localhost:" + port);

            while (!token.IsCancellationRequested)
            {
                string message = server.ReceiveFrameString();

                message = message.Replace(",", ".");

                var vector = message.Split(';').ToArray();

                object result = Project.Predict(strModelToEvaluatePath, vector.ToArray(), pd);

                server.SendFrame(result.ToString());
            }
        }
    }
}
